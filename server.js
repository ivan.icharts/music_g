const express = require('express')
const path = require('path')

const app = express()

console.log('NODE ENV', process.env.NODE_ENV);

var pathHtml, config
if (process.env.NODE_ENV === 'production') {
  console.log('PRODUCTION MODE')

  pathHtml = path.join(__dirname, 'static/index.html')
  config = require('./config/production.json')
} else {
  console.log('DEBUG MODE')

  pathHtml = path.join(__dirname, 'static/index.dev.html')
  config = require('./config/default.json')
}

app.use('/assets', express.static(path.join(__dirname, 'static/assets')))
app.use('/dist', express.static(path.join(__dirname, 'static/dist')))

app.get('*', (req, res) => res.sendFile(pathHtml))

const port = config.port

app.listen(port, function(error) {
  if (error)
    console.error(error)
  else
    console.info('==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.', port, port)
})
