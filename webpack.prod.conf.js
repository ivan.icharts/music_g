var path    = require('path');
var webpack = require('webpack');
var CleanPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var marked = require('marked');
var renderer = new marked.Renderer();
var config = require('./config/production.json')

var relativeAssetsPath = './static/dist';
var assetsPath = path.join(__dirname, relativeAssetsPath);

module.exports = {
  entry: [
    'babel-polyfill',
    './src/index',
  ],
  output: {
    path: assetsPath,
    filename   : 'bundle.js',   
    publicPath : '/dist/'
  },
  progress: true,
  module  : {
    loaders: [{
      test    : /\.js$/,
      loaders : ['babel'],
      exclude : [/node_modules/]
    }, {
      test    : /\.css$/,
      loader: ExtractTextPlugin.extract('style', "css")
    }, {
      test: /\.module.scss$/,
      loader: ExtractTextPlugin.extract('style', 'css?modules&importLoaders=2&sourceMap!autoprefixer?browsers=last 2 version!sass?outputStyle=expanded&sourceMap=true&sourceMapContents=true')
    }, {
      test: /^((?!\.module).)*scss$/,
      loader: ExtractTextPlugin.extract('style', 'css-loader!sass-loader')
    }, {
      test: /\.html$/,
      loader: 'file-loader?name=[path][name].[ext]!extract-loader!html-loader'
    }, {
      test: /\.md$/,
      loader: "html!markdown?gfm=true&tables=true&breaks=false&pedantic=false&sanitize=false&smartLists=true&smartypants=false"
    }, {
      test: /.json$/,
      loader: 'json'
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=image/svg+xml"
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=application/octet-stream"
    }, {
      test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=application/font-woff"
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: "file",
    }]
  },
  markdownLoader: {
      renderer: renderer
  },
  plugins: [
     // css files from the extract-text-plugin loader
    new ExtractTextPlugin('style.css'),
    // ignore dev config
    new webpack.IgnorePlugin(/\.\/dev/, /\/config$/),
    new webpack.ProvidePlugin({
        'React': 'react'
    }),
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: JSON.stringify('production')
      },
      Config: JSON.stringify(config)
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      output: {
        comments: false
      }
    })
  ],
  resolve: {
    root: __dirname,
    modulesDirectories: [
      'src',
      'node_modules'
    ],
    modules: ['node_modules', 'src', 'static/assets'],
    extensions: ['', '.json', '.js'],
    alias: { }
  }
};