import * as c from 'constants/'

const initialState = {
  data: [],
  fetching: false,
  error: false
}

export default function app(state = initialState, action) {
  switch (action.type) {
    case c.EXAMPLE_API_REQUEST:
      return {...state, fetching: true}
    case c.EXAMPLE_API_SUCCESS:
      return {...state, data: action.payload, fetching: false}
    case c.EXAMPLE_API_ERROR:
      return {...state, fetching: false, error: action.payload || true}
    default:
      return state
  }
}
