import * as c from 'constants/'

const initialState = {
  tracks: []
}

export default function app(state = initialState, action) {
  switch (action.type) {
    case c.API_GETTRACKS_SUCCESS:
      return {...state, tracks: action.payload}
    default:
      return state
  }
}
