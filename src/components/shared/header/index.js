import { PureComponent } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import s from './header.module.scss'
import * as t from '../../../actions/track'

// console.log(s,);
@connect(mapStateToProps, mapDispatchToProps)
export default class Header extends PureComponent {

  search = (e) => {
    e.preventDefault()
    const q = e.target.elements[0].value
    console.log(q, this.props.getNew)
    this.props.getNew({q})
    // return false
  }

  render = () => (
    <header className={s['header']}>
      <div onClick={this.props.getNew.bind(null, {})} className={s['logo']}>
        Music Archive
      </div>

      <div className='current-category'>
        <form onSubmit={this.search}>
          <input className={s['search']} placeholder='search...' type='text'/>
        </form>
      </div>
    </header>
  )
}

function mapStateToProps(state) {
  return {
    app: state.rootReducer.app,
    tracks: state.rootReducer.track.tracks
  }
}
function mapDispatchToProps(dispatch) {
  return {
    // exampleFunc: bindActionCreators(exampleFunc, dispatch),
    getNew: bindActionCreators(t.getNew, dispatch)
  }
}
