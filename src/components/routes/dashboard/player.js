
import './style.scss'

const pause = (player) => () => player.pause()
const play = player => () => player.play()

export default () => (
  (<div id='player'></div>)
)
