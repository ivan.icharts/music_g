import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import sc from 'soundcloud'

import { exampleFunc } from 'actions/app'
import Player from './player'
import * as t from '../../../actions/track.js'

import styles from './style.module.scss'
console.log(styles);

// http://ws.audioscrobbler.com/2.0/?method=chart.gettoptracks&api_key=523529d3846fdc340f75982082ef9766&format=json
// http://www.last.fm/api/show/chart.getTopTracks
// f4323c6f7c0cd73d2d786a2b1cdae80c
const defaultSrc = 'https://cdn.dribbble.com/users/41394/screenshots/222354/music-town.jpg'

@connect(mapStateToProps, mapDispatchToProps)
export default class ExampleComponent extends Component {

  state = {
    tracks: [],
    player: null,
    track: null
  }

  componentDidMount() {
    // this.props.exampleFunc()
    this.props.getNew()
    // sc.initialize({client_id: 'f4323c6f7c0cd73d2d786a2b1cdae80c'})
    // sc.get('/tracks')
    //   .then(tracks => this.setState({tracks}))

    console.log('SC', sc);
  }

  playSongHandler = track => () => (
    console.log(track),
    this.setState({track: track.id}, () => sc.oEmbed(track.permalink_url, {element: document.getElementById('player')}))
    )
  // sc.stream(`/tracks/${id}`)
  //   .then(player => (this.setState({player}), player.play(), player.on('time', (...e) => console.log(e))))
  //   .catch(console.log)

  pauseSongHandler = () => this.state.player.pause()

  closePopup = () => this.setState({track: null})

  onImageError = function (e) {
    // console.log(this, e.target.src)
    e.target.src = defaultSrc
  }

  render() {
    const { player, track } = this.state
    const { tracks } = this.props

    return (
      <div className={styles['tracks-wrapper']}>
        {tracks.map(track => (
          <div key={track.id} className={styles['track']}>
            <div onClick={this.playSongHandler(track)} className={styles['track-img']}>
              <img src={track.artwork_url || defaultSrc} onError={this.onImageError} alt=''/>
            </div>
            <strong className={styles['title']}>
              {track.title}
            </strong>
            <div className={styles['author']}>
              {track.user.username}
            </div>
          </div>
        ))}

        {track && <div className='player-wrapper2'>
           <div onClick={this.closePopup} className='close'>&#10006;</div>
          <Player player={player}></Player>
        </div>}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    app: state.rootReducer.app,
    tracks: state.rootReducer.track.tracks
  }
}
function mapDispatchToProps(dispatch) {
  return {
    // exampleFunc: bindActionCreators(exampleFunc, dispatch),
    getNew: bindActionCreators(t.getNew, dispatch)
  }
}
// export default connect(mapStateToProps, mapDispatchToProps)(ExampleComponent)
