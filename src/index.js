import 'babel-polyfill'
import 'whatwg-fetch'
import React from 'react'
import { Provider } from 'react-redux'
import { render } from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'

import Routes from 'utils/routes'
import configureStore from 'utils/store'

const store = configureStore()
const history = syncHistoryWithStore(browserHistory, store)

const renderApp = AppRoutes => {
  render(
    <Provider store={store}>
      <AppRoutes history={history} />
    </Provider>,
    document.getElementById('root')
  )
}

renderApp(Routes)

if (module.hot) {
  module.hot.accept('utils/routes', () => {
    const newRoutes = require('utils/routes').default
    renderApp(newRoutes)
  });
}
