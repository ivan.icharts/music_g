import { createStore, applyMiddleware, combineReducers } from 'redux'
import { browserHistory } from 'react-router'
import { routerMiddleware, routerReducer } from 'react-router-redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'

import rootReducer from 'reducers/'

/* global Config */

export default function configureStore() {

  const middlewares = [
      routerMiddleware(browserHistory),
      thunkMiddleware
  ]
  Config.debug && middlewares.push(createLogger())

  const store = createStore(
    combineReducers({
      rootReducer,
      routing: routerReducer
    }),
    applyMiddleware(...middlewares)
  )

  /*if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers/', () => {
      const nextRootReducer = require('../reducers/');
      store.replaceReducer(nextRootReducer);
    });
  }*/

  return store
}
