import { Router, Route, Redirect, IndexRedirect } from 'react-router'

import App from 'containers/App'
import { Dashboard } from 'components/'

import * as r from 'constants/routes'
//import * as r from 'сonstants/routes'


import 'style/main.scss'

export default (props) => (
  <Router history={props.history}>
    <Route            path={r.INDEX} component={App}>
      <IndexRedirect  to={r.DASHBOARD}    />

      <Route          path={r.DASHBOARD}  component={Dashboard} />

      <Redirect       from={r.ANY} to={r.INDEX}   />
    </Route>
  </Router>
)
