import sc from '../constants/sc'

import * as c from '../constants'

export const getNew = (params={}) => dispatch => sc.get('/tracks', params)
  .then(tracks => dispatch({
    type: c.API_GETTRACKS_SUCCESS,
    payload: tracks
  }))

// export const search = params => dispatch => sc.get('/tracks', params)
