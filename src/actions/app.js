import * as c from 'constants/'

export const exampleFunc = () => dispatch => {
  dispatch({ type: c.EXAMPLE_API_REQUEST })
  fetch(c.EXAMPLE_API_URL)
    .then(response => {
      if (response.status === 200)
        return response.json()
      else 
        return Error()
    })
    .then(result => dispatch({
      type: c.EXAMPLE_API_SUCCESS,
      payload: result
    }))
    .catch(() => dispatch({ type: c.EXAMPLE_API_ERROR }))
}