import { Component } from 'react'

import Header from '../components/shared/header'

export default class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <main>{ this.props.children }</main>
        <footer>footer</footer>
      </div>
    )
  }
}
