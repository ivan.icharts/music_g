export const API_URL = 'http://ws.audioscrobbler.com/2.0/'
export const API_KEY = '523529d3846fdc340f75982082ef9766'

export const API_TOPCHARTS_URL     = `${API_URL}?method=chart.gettoptracks&api_key=${API_KEY}&format=json`
export const API_TOPCHARTS_SUCCESS = 'API_TOPCHARTS_SUCCESS'
export const API_TOPCHARTS_FAIL    = 'API_TOPCHARTS_FAIL'

export const EXAMPLE_API_REQUEST  = 'EXAMPLE_API_REQUEST'
export const EXAMPLE_API_SUCCESS  = 'EXAMPLE_API_SUCCESS'
export const EXAMPLE_API_ERROR    = 'EXAMPLE_API_ERROR'

export const API_GETTRACKS_SUCCESS = 'API_GETTRACKS_SUCCESS' 
