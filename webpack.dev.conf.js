const path    = require('path')
const webpack = require('webpack')
const marked = require('marked')
const renderer = new marked.Renderer()
const config = require('./config/default.json')

module.exports = {
  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:3333',
    'webpack/hot/only-dev-server',
    './src/'
  ],
  output: {
    filename   : 'bundle.js',
    path       : path.join(__dirname),
    publicPath : 'http://localhost:3333/static/'
  },
  devtool : 'eval',
  devServer: {
   headers: { "Access-Control-Allow-Origin": "*" },
   publicPath: "http://localhost:3333/static/"
  },
  debug   : true,
  watch   : true,
  module  : {
    loaders: [{
      test    : /\.js$/,
      loaders : ['babel-loader'],
      exclude : [/node_modules/]
    }, {
      test    : /\.css$/,
      loader  : 'style!css'
    }, {
      test: /\.module.scss$/,
      loader: 'style-loader!css-loader?modules&importLoaders=2&sourceMap&localIdentName=[local]___[hash:base64:5]!autoprefixer?browsers=last 2 version!sass?outputStyle=expanded&sourceMap'
    }, {
      test: /^((?!\.module).)*scss$/,
      loader: 'style!css!sass'
    },{
      test: /\.html$/,
      loader: 'file-loader?name=[path][name].[ext]!extract-loader!html-loader'
    }, {
      test: /\.md$/,
      loader: "html!markdown?gfm=true&tables=true&breaks=false&pedantic=false&sanitize=false&smartLists=true&smartypants=false"
    }, {
      test: /.json$/,
      loader: 'json'
    }, {
      test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=image/svg+xml"
    }, {
      test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=application/octet-stream"
    }, {
      test: /\.woff(2)?(\?v=\d+\.\d+\.\d+)?$/,
      loader: "url?limit=10000&mimetype=application/font-woff"
    }, {
      test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
      loader: "file",
    }]
  },
  markdownLoader: {
    renderer: renderer
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProvidePlugin({
      'React': 'react'
    }),
    new webpack.DefinePlugin({
      Config: JSON.stringify(config)
    }),
  ],
  resolve: {
    root: path.resolve('./src'),
    modules: ['node_modules', 'src', 'static/assets'],
    extensiions: ['', 'js']
  }
};
